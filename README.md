# Práctico_1_Función_en_python



# Adivina el Número

[Adivina](https://gitlab.com/vision-por-computadora615916/practico_1_funcion_en_python/-/blob/main/adivina.py?ref_type=heads) es un proyecto simple en Python donde puedes jugar a adivinar un número secreto generado por el programa. 


## Cómo jugar

1. El número secreto debe estar entre 0 y 100, y debe ser generado dentro de la función.
2. Si el usuario adivinó el número secreto antes de superar la cantidad permitida de
intentos, imprime un mensaje con el número de intentos en los que adivinó
3. En caso de que esta cantidad de intentos sea superada el programa debe terminar
con un mensaje.

¡Diviértete y buena suerte tratando de adivinar el número!
