import random

def adivino(inten):
	
	num_aleator = random.randint(0,100)
	num_ingresado = int(input('Ingrese un numero:'))

	if num_ingresado == num_aleator:
		print('Correcto, acertaste el numero en el intento', inten)
		inten = 6
		return (inten)
	else:
		print('Incorrecto, el numero es',num_aleator)
		inten += 1
		return (inten)

intento = 1

while intento != 6:
	intento = adivino(intento)
	
